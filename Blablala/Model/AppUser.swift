//
//  AppUser.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import Foundation

final class AppUser: NSObject, NSCoding {
	
	var user : User
	
	var identifier : Int { return user.identifier }
	var nickname : String { return user.nickname }
	var avatarUrl : URL? { return user.avatarUrl }
	
	init(identifier : Int, nickname : String) {
		user = User(identifier: identifier, nickname: nickname)
	}
	
	required init(coder decoder: NSCoder) {
		let identifier = decoder.decodeInteger(forKey: "identifier")
		let nickname = decoder.decodeObject(forKey: "nickname") as? String ?? ""
		let avatarUrlStr = decoder.decodeObject(forKey: "avatarUrl") as? String ?? ""
		user = User(identifier: identifier, nickname: nickname, avatarUrl: URL(string: avatarUrlStr))
	}
	
	func encode(with coder: NSCoder) {
		coder.encode(user.identifier, forKey: "identifier")
		coder.encode(user.nickname, forKey: "nickname")
		coder.encode(user.avatarUrl?.absoluteString, forKey : "avatarUrl")
	}
}
