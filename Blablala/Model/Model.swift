//
//  Model.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import SwiftyJSON

protocol Model {
	init(json: JSON)
}
