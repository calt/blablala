//
//  User.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import SwiftyJSON

class User : Model {
	
	var identifier : Int
	var nickname : String
	var avatarUrl : URL?
	
	required init(json: JSON) {
		identifier = json["id"].intValue
		nickname = json["nickname"].stringValue
		avatarUrl = URL(string: json["avatarUrl"].stringValue)
	}
	
	init(identifier : Int, nickname : String, avatarUrl : URL? = URL(string : "https://i.imgur.com/85asSHY.jpg")) {
		self.identifier = identifier
		self.nickname = nickname
		self.avatarUrl = avatarUrl
	}
}
