//
//  Message.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import SwiftyJSON

class Message : Model {
	
	var identifier : Int
	var text : String
	var time : Date
	var user : User
	
	required init(json: JSON) {
		identifier = json["identifier"].intValue
		text = json["text"].stringValue
		let timestamp = json["timestamp"].intValue
		time = Date(timeIntervalSince1970: TimeInterval(timestamp))
		user = User(json: json["user"])
	}
	
	init(identifier : Int, text : String, time : Date = Date(), user : User) {
		self.identifier = identifier
		self.text = text
		self.time = time
		self.user = user
	}
}
