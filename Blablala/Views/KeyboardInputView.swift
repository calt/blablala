//
//  KeyboardInputView.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 25/02/2017.
//  Copyright © 2017 dembrew. All rights reserved.
//

import UIKit

class KeyboardInputView: UIView {

	private lazy var textView = UITextView()
	private lazy var sendButton = UIButton()
	fileprivate lazy var placeholderLabel = UILabel()
	
	var text : String? { return textView.text }
	var placeholder : String? {
		get {
			return placeholderLabel.text
		}
		set {
			placeholderLabel.text = newValue
		}
	}
	var sendAction : ((_ text : String?) -> Void)?
	
	var didSetupSubviews: Bool
	var didSetupConstraints: Bool
	
	init() {
		didSetupSubviews = false
		didSetupConstraints = true
		super.init(frame: CGRect.zero)
		backgroundColor = #colorLiteral(red: 0.6130953431, green: 0.7653919458, blue: 0.9121891856, alpha: 1)
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("NSCoding not supported")
	}
	
	override func layoutSubviews() {
		if(!didSetupSubviews) {
			setupSubviews()
			didSetupConstraints = false
			self.setNeedsUpdateConstraints()
		}
		super.layoutSubviews()
	}
	
	override func updateConstraints() {
		if(!self.didSetupConstraints) {
			setupConstraints()
		}
		super.updateConstraints()
	}
	
	private func setupSubviews() {
		
		textView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		textView.autocorrectionType = .no
		textView.isScrollEnabled = false
		textView.layer.cornerRadius = 3
		textView.delegate = self
		textView.clipsToBounds = true
		addSubview(textView)
		
		placeholderLabel.text = "Enter your message".localized
		placeholderLabel.font = UIFont.systemFont(ofSize: 12)
		placeholderLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
		addSubview(placeholderLabel)

		sendButton.backgroundColor = #colorLiteral(red: 0.2229198217, green: 0.5136197209, blue: 0.7861803174, alpha: 1)
		sendButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
		sendButton.setTitle("Send".localized, for: .normal)
		sendButton.layer.cornerRadius = 15
		sendButton.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1).cgColor
		sendButton.clipsToBounds = true
		sendButton.addTarget(self, action: #selector(KeyboardInputView.sendButtonTapped(sender:)), for: .touchUpInside)
		addSubview(sendButton)
		
		didSetupSubviews = true
	}
	
	private func setupConstraints() {
		
		textView.snp.makeConstraints { make in
			make.left.equalTo(self).offset(5)
			make.top.equalTo(self).offset(5)
			make.bottom.equalTo(self).offset(-5)
			make.height.greaterThanOrEqualTo(30)
			make.right.equalTo(sendButton.snp.left).offset(-5)
		}
		
		placeholderLabel.snp.makeConstraints { make in
			make.centerY.equalTo(textView)
			make.left.equalTo(textView).offset(5)
		}
		
		sendButton.snp.makeConstraints { make in
			make.right.equalTo(self).offset(-5)
			make.bottom.equalTo(self).offset(-5)
			make.size.equalTo(CGSize(width: 75, height: 30))
		}
		
		didSetupConstraints = true
	}
	
	
	@objc private func sendButtonTapped(sender :UIButton) {
		sendAction?(text)
		textView.text = nil
	}
	
	func stopEditing() {
		textView.resignFirstResponder()
	}
}

extension KeyboardInputView : UITextViewDelegate {
	func textViewDidChange(_ textView: UITextView) {
		placeholderLabel.isHidden = textView.hasText
	}
	
	func textViewDidBeginEditing(_ textView: UITextView) {
		placeholderLabel.isHidden = true
	}
	
	func textViewDidEndEditing(_ textView: UITextView) {
		placeholderLabel.isHidden = textView.hasText
	}
}
