//
//  IncomingMessageTableViewCell.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit
import Kingfisher

class IncomingMessageTableViewCell: UITableViewCell {
	
	lazy var avatarImageView = UIImageView()
	lazy var nicknameLabel = UILabel()
	lazy var messageContainerView = UIView()
	lazy var messageLabel = UILabel()
	lazy var timeLabel = UILabel()
	
	var didSetupSubviews: Bool
	var didSetupConstraints: Bool
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		didSetupSubviews = false
		didSetupConstraints = true
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("NSCoding not supported")
	}
	
	override func layoutSubviews() {
		if(!didSetupSubviews) {
			setupSubviews()
			didSetupConstraints = false
			self.setNeedsUpdateConstraints()
		}
		super.layoutSubviews()
	}
	
	override func updateConstraints() {
		if(!self.didSetupConstraints) {
			setupConstraints()
		}
		super.updateConstraints()
	}
	
	private func setupSubviews() {
		
		avatarImageView.contentMode = .scaleAspectFit
		avatarImageView.clipsToBounds = true
		avatarImageView.backgroundColor = #colorLiteral(red: 0.2387676239, green: 0.6746522784, blue: 0.9680932164, alpha: 1)
		contentView.addSubview(avatarImageView)
		
		nicknameLabel.font = UIFont.boldSystemFont(ofSize: 15)
		contentView.addSubview(nicknameLabel)
		
		messageContainerView.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
		messageContainerView.layer.cornerRadius = 10
		messageContainerView.layer.borderColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1).cgColor
		messageContainerView.layer.borderWidth = 1
		messageContainerView.clipsToBounds = true
		contentView.addSubview(messageContainerView)
		
		messageLabel.font = UIFont.systemFont(ofSize: 15, weight: 0.15)
		messageLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		messageLabel.numberOfLines = 0
		messageContainerView.addSubview(messageLabel)
		
		timeLabel.font = UIFont.systemFont(ofSize: 12, weight: 0.1)
		timeLabel.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
		timeLabel.textAlignment = .center
		contentView.addSubview(timeLabel)
		
		didSetupSubviews = true
	}
	
	private func setupConstraints() {
		
		avatarImageView.snp.makeConstraints { (make) in
			make.left.equalTo(contentView).offset(5)
			make.top.equalTo(contentView).offset(5)
			make.size.equalTo(CGSize(width: 60, height: 60))
		}
		
		nicknameLabel.snp.makeConstraints { (make) in
			make.top.equalTo(contentView).offset(3)
			make.left.equalTo(avatarImageView.snp.right).offset(5)
			make.right.equalTo(contentView).offset(-5)
		}
		
		messageContainerView.snp.makeConstraints { (make) in
			make.top.equalTo(nicknameLabel.snp.bottom).offset(5)
			make.left.equalTo(avatarImageView.snp.right).offset(5)
			make.right.lessThanOrEqualTo(contentView).offset(-25)
		}
		
		messageLabel.snp.makeConstraints { make in
			make.edges.equalTo(messageContainerView).inset(UIEdgeInsetsMake(10, 7, 10, 7))
		}
		
		timeLabel.snp.makeConstraints { (make) in
			make.top.equalTo(messageContainerView.snp.bottom).offset(2)
			make.left.equalTo(contentView)
			make.right.equalTo(contentView)
			make.bottom.equalTo(contentView).offset(-5)
		}
		didSetupConstraints = true
	}
}

extension IncomingMessageTableViewCell : MessageTableViewCell {
	func configure(withModel model : MessageTableViewCellModel) {
		avatarImageView.kf.setImage(with: model.avatarURL)
		nicknameLabel.text = model.nicknameText
		messageLabel.text = model.messageText
		timeLabel.text = model.timeText
		messageLabel.sizeToFit()
		self.layoutIfNeeded()
	}
}
