//
//  NicknameViewModel.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

class NicknameViewModel {
	
	var showError : ((_ show : Bool) -> Void)?
	
	var nickname : String? {
		didSet {
			self.showError?(!canContinue)
		}
	}
	
	private var canContinue : Bool {
		guard let nickname = nickname else { return false }
		return nickname.trimmingCharacters(in: .whitespacesAndNewlines).characters.count > 2
	}
	
	func continueButtonTapped() {
		guard let nickname = nickname, canContinue else {
			self.showError?(true)
			return
		}
		self.showError?(false)
		let user = AppUser(identifier: -1, nickname: nickname) // ideally created on the backend using a 'service', but I've simplified it for this test. 
		AppCoordinator.shared.showMessages(for: user)
	}
}
