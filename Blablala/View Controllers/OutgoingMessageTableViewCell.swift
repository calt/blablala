//
//  OutgoingMessageTableViewCell.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit
import Kingfisher

class OutgoingMessageTableViewCell: UITableViewCell {
	lazy var avatarImageView = UIImageView()
	lazy var nicknameLabel = UILabel()
	lazy var messageContainerView = UIView()
	lazy var messageLabel = UILabel()
	lazy var timeLabel = UILabel()
	
	var didSetupSubviews: Bool
	var didSetupConstraints: Bool
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		didSetupSubviews = false
		didSetupConstraints = true
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("NSCoding not supported")
	}
	
	override func layoutSubviews() {
		if(!didSetupSubviews) {
			setupSubviews()
			didSetupConstraints = false
			self.setNeedsUpdateConstraints()
		}
		super.layoutSubviews()
	}
	
	override func updateConstraints() {
		if(!self.didSetupConstraints) {
			setupConstraints()
		}
		super.updateConstraints()
	}
	
	private func setupSubviews() {
		avatarImageView.contentMode = .scaleAspectFit
		avatarImageView.clipsToBounds = true
		avatarImageView.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
		contentView.addSubview(avatarImageView)
		
		nicknameLabel.font = UIFont.boldSystemFont(ofSize: 15)
		nicknameLabel.textAlignment = .right
		contentView.addSubview(nicknameLabel)
		
		messageContainerView.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
		messageContainerView.layer.cornerRadius = 10
		messageContainerView.layer.borderColor = #colorLiteral(red: 0.2734897137, green: 0.4848722816, blue: 0.139934659, alpha: 1).cgColor
		messageContainerView.layer.borderWidth = 1
		messageContainerView.clipsToBounds = true
		contentView.addSubview(messageContainerView)
		
		messageLabel.font = UIFont.systemFont(ofSize: 15, weight: 0.15)
		messageLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		messageLabel.numberOfLines = 0
		messageContainerView.addSubview(messageLabel)
		
		timeLabel.font = UIFont.systemFont(ofSize: 12, weight: 0.1)
		timeLabel.textColor = #colorLiteral(red: 0.803850472, green: 0.8039670587, blue: 0.8038250804, alpha: 1)
		timeLabel.textAlignment = .center
		contentView.addSubview(timeLabel)
		
		didSetupSubviews = true
	}
	
	private func setupConstraints() {
		
		avatarImageView.snp.makeConstraints { (make) in
			make.right.equalTo(contentView).offset(-5)
			make.top.equalTo(contentView).offset(5)
			make.size.equalTo(CGSize(width: 60, height: 60))
		}
		
		nicknameLabel.snp.makeConstraints { (make) in
			make.top.equalTo(contentView).offset(3)
			make.right.equalTo(avatarImageView.snp.left).offset(-5)
			make.left.equalTo(contentView).offset(5)
		}
		
		messageContainerView.snp.makeConstraints { (make) in
			make.top.equalTo(nicknameLabel.snp.bottom).offset(5)
			make.right.equalTo(avatarImageView.snp.left).offset(-5)
			make.left.greaterThanOrEqualTo(contentView).offset(25)
		}
		
		messageLabel.snp.makeConstraints { make in
			make.edges.equalTo(messageContainerView).inset(UIEdgeInsetsMake(10, 7, 10, 7))
		}
		
		timeLabel.snp.makeConstraints { (make) in
			make.top.equalTo(messageContainerView.snp.bottom).offset(2)
			make.left.equalTo(contentView)
			make.right.equalTo(contentView)
			make.bottom.equalTo(contentView).offset(-5)
		}
		
		didSetupConstraints = true
	}
}

extension OutgoingMessageTableViewCell : MessageTableViewCell {
	func configure(withModel model : MessageTableViewCellModel) {
		avatarImageView.kf.setImage(with: model.avatarURL)
		nicknameLabel.text = model.nicknameText
		messageLabel.text = model.messageText
		timeLabel.text = model.timeText
		messageLabel.sizeToFit()
		self.layoutIfNeeded()
	}
}
