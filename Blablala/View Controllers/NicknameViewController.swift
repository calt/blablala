//
//  NicknameViewController.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit
import SnapKit

class NicknameViewController: UIViewController {
	
	fileprivate var model : NicknameViewModel
	
	private lazy var errorLabel = UILabel()
	private lazy var textFieldContainer = UIView()
	private lazy var nicknameTextField = UITextField()
	private lazy var continueButton = UIButton()
	
	init(model : NicknameViewModel) {
		self.model = model
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("NSCoding not suppported")
	}
	
	override func loadView() {
		setupViews()
		setupConstraints()
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		title = "Enter Your Nickname".localized
		
		model.showError = { [weak self] show in
			self?.showError(show)
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		nicknameTextField.becomeFirstResponder()
		navigationController?.setNavigationBarHidden(true, animated: false)
	}
	
	override func viewDidLayoutSubviews() {
		continueButton.applyGradient(colors: [#colorLiteral(red: 0.8086157441, green: 0.8849133849, blue: 0.956246078, alpha: 1),#colorLiteral(red: 0.8086157441, green: 0.8849133849, blue: 0.956246078, alpha: 1),#colorLiteral(red: 0.6170199513, green: 0.7693166137, blue: 0.9161146283, alpha: 1)], locations: [0.0,0.5,1.0])
		super.viewDidLayoutSubviews()
	}
	
	//MARK: Layout
	private func setupViews() {
		view = UIView()
		view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
		
		errorLabel.font = UIFont.boldSystemFont(ofSize: 10)
		errorLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
		errorLabel.alpha = 0
		errorLabel.textAlignment = .center
		errorLabel.text = "Nickname should be longer than 2 characters.".localized
		view.addSubview(errorLabel)
		
		textFieldContainer.layer.cornerRadius = 20
		textFieldContainer.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1).cgColor
		textFieldContainer.layer.borderWidth = 1
		textFieldContainer.clipsToBounds = true
		view.addSubview(textFieldContainer)
		
		nicknameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
		nicknameTextField.leftViewMode = .always
		nicknameTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
		nicknameTextField.rightViewMode = .always
		nicknameTextField.autocorrectionType = .no
		nicknameTextField.textAlignment = .center
		nicknameTextField.contentVerticalAlignment = .center
		nicknameTextField.font = UIFont.systemFont(ofSize: 20)
		nicknameTextField.placeholder = "Nickname".localized
		nicknameTextField.addTarget(self, action: #selector(NicknameViewController.textFieldTextChanged(sender:)), for: .editingChanged)
		textFieldContainer.addSubview(nicknameTextField)
		
		continueButton.setTitleColor(#colorLiteral(red: 0.09496370703, green: 0.2809436023, blue: 0.45710361, alpha: 1), for: .normal)
		continueButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
		continueButton.setTitle("Continue".localized, for: .normal)
		continueButton.layer.borderWidth = 1
		continueButton.layer.borderColor = #colorLiteral(red: 0.4759489298, green: 0.8372092843, blue: 0.9759084582, alpha: 1).cgColor
		continueButton.layer.cornerRadius = 4
		continueButton.clipsToBounds = true
		continueButton.addTarget(self, action: #selector(NicknameViewController.continueButtonTapped(sender:)), for: .touchUpInside)
		view.addSubview(continueButton)
	}
	
	private func setupConstraints() {
		
		textFieldContainer.snp.makeConstraints { make in
			make.leading.equalTo(view).offset(40)
			make.trailing.equalTo(view).offset(-40)
			make.height.equalTo(40)
			make.centerY.equalTo(view).offset(-100)
		}
		
		nicknameTextField.snp.makeConstraints { make in
			make.leading.equalTo(textFieldContainer)
			make.trailing.equalTo(textFieldContainer)
			make.center.equalTo(textFieldContainer)
		}
		
		errorLabel.snp.makeConstraints { make in
			make.leading.equalTo(textFieldContainer)
			make.trailing.equalTo(textFieldContainer)
			make.bottom.equalTo(textFieldContainer.snp.top).offset(-15)
		}
		
		continueButton.snp.makeConstraints { make in
			make.leading.equalTo(nicknameTextField)
			make.trailing.equalTo(nicknameTextField)
			make.height.equalTo(45)
			make.top.equalTo(nicknameTextField.snp.bottom).offset(15)
		}
	}
	
	//MARK: Actions
	@objc private func continueButtonTapped(sender : UIButton) {
		model.continueButtonTapped()
	}
	
	@objc private func textFieldTextChanged(sender : UITextField) {
		model.nickname = nicknameTextField.text
	}
	
	func showError(_ show : Bool) {
		if show {
			if errorLabel.alpha == 0 {
				UIView.animate(withDuration: 0.5, animations: {
					self.errorLabel.alpha = 1
				})
			}
		} else {
			errorLabel.alpha = 0
		}
	}
}
