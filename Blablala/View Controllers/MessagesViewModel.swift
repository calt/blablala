//
//  MessagesViewModel.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 dembrew. All rights reserved.
//

class MessagesViewModel {
	
	var title : String?
	var modelDidUpdate : (() -> Void)?
	var messageAdded : (() -> Void)?
	
	private var service : MessageServiceProtocol
	private var user : AppUser
	private var messages : [Message]?
	
	init(service : MessageServiceProtocol, user : AppUser) {
		self.service = service
		self.user = user
		self.title = user.user.nickname
	}
	
	func viewLoaded() {
		service.getMessages { messages, success, error in
			self.messages = messages
			self.modelDidUpdate?()
		}
	}
	
	func sendMessage(_ text : String) {
		let sender = user.user
		service.sendMessage(text, sender: sender) { message, success, error in
			guard let message = message, success == true, error == nil else { return }
			if self.messages == nil {
				self.messages = []
			}
			self.messages?.append(message)
			self.messageAdded?()
		}
	}
	
	func leave() {
		AppCoordinator.shared.logout()
	}
	
	var rowCount : Int { return messages?.count ?? 0 }
	
	func modelForRow(row : Int) -> MessageTableViewCellModel {
		assert(messages != nil, "Messages should not be nil while creating a cell model")
		assert(row <= messages!.count, "Must have enough objects in the array")
		let message = messages![row]
		let messageType : MessageType = message.user.identifier == user.identifier ? .outgoing : .incoming
		return MessageTableViewCellModel(message: message, type: messageType)
	}

}
