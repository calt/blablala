//
//  MessageTableViewCellModel.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 24/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import Foundation

class MessageTableViewCellModel {
	
	private(set) var nicknameText : String
	private(set) var messageText : String
	private(set) var avatarURL : URL?
	private(set) var timeText : String
	private(set) var messageType : MessageType
	
	private var message : Message
	
	init(message : Message, type : MessageType) {
		self.message = message
		self.messageType = type		
		nicknameText = message.user.nickname
		messageText = message.text
		avatarURL = message.user.avatarUrl
		timeText = message.time.formatted
	}

}

enum MessageType : String {
	case incoming = "Incoming"
	case outgoing = "Outgoing"
}
