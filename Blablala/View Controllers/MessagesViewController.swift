//
//  MessagesViewController.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit
import SnapKit

class MessagesViewController: UIViewController {
	
	private lazy var tableView : UITableView = UITableView()
	fileprivate let incomingCellIdentifier = "IncomingMessageTableViewCell"
	fileprivate let outgoingCellIdentifier = "OutgoingMessageTableViewCell"
	
	fileprivate lazy var keyboardInputView = KeyboardInputView()
	
	private var inputBottomConstraint : Constraint?
	
	fileprivate var model : MessagesViewModel
	
	override func loadView() {
		setupViews()
		setupConstraints() //Constraints added here, not in updateViewConstraints, because we are adding them for the first time, not updating them. https://developer.apple.com/reference/uikit/uiviewcontroller/1621379-updateviewconstraints
	}
	
	init(model : MessagesViewModel) {
		self.model = model
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("NSCoding not suppported")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.register(IncomingMessageTableViewCell.self, forCellReuseIdentifier: incomingCellIdentifier)
		tableView.register(OutgoingMessageTableViewCell.self, forCellReuseIdentifier: outgoingCellIdentifier)
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Leave".localized, style: .plain, target: self, action: #selector(MessagesViewController.leaveButtonTapped(sender:)))
		
		// Update views with the model data
		title = model.title
		model.modelDidUpdate = { [weak self] in
			self?.tableView.reloadData()
		}
		
		model.messageAdded = { [weak self] in
			guard let `self` = self else { return }
			let indexPath = IndexPath(row: self.model.rowCount - 1, section: 0)
//			self.tableView.beginUpdates()
//			self.tableView.insertRows(at: [indexPath], with: .none)
//			self.tableView.endUpdates() //this starts to glitch after inserting a few rows,falling back to reloadData() 🤔
			self.tableView.reloadData()
			self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
		}
		
		keyboardInputView.sendAction = { [weak self] text in
			guard let text = text, text.characters.count > 0 else { return }
			self?.model.sendMessage(text)
		}
		
		model.viewLoaded()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(false, animated: false)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		NotificationCenter.default.addObserver(self, selector: #selector(MessagesViewController.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		NotificationCenter.default.removeObserver(self)
	}
	
	//MARK: Layout
	private func setupViews() {
		view = UIView()
		view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		
		tableView.dataSource = self
		tableView.delegate = self
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 170
		tableView.separatorStyle = .none
		tableView.allowsSelection = false
		view.addSubview(tableView)
		
		view.addSubview(keyboardInputView)
	}
	
	private func setupConstraints() {
		tableView.snp.makeConstraints { make in
			make.left.equalTo(view)
			make.right.equalTo(view)
			make.top.equalTo(view)
			make.bottom.equalTo(keyboardInputView.snp.top)
		}
		
		keyboardInputView.snp.makeConstraints { make in
			make.left.equalTo(view)
			make.right.equalTo(view)
			make.height.greaterThanOrEqualTo(30)
			self.inputBottomConstraint = make.bottom.equalTo(view).offset(0).constraint
		}
	}
	
	//MARK : Actions
	@objc private func leaveButtonTapped(sender : UIBarButtonItem) {
		model.leave()
	}
	
	//MARK : Handle Keyboard
	func keyboardNotification(notification: NSNotification) {
		if let userInfo = notification.userInfo {
			let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
			let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
			let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
			let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
			let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
			if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
				self.inputBottomConstraint?.update(offset: 0)
			} else {
				let offset = -(endFrame?.size.height ?? 0.0)
				self.inputBottomConstraint?.update(offset: offset)
			}
			UIView.animate(withDuration: duration,
			               delay: TimeInterval(0),
			               options: animationCurve,
			               animations: { self.view.layoutIfNeeded() },
			               completion: nil)
			
		}
	}
}

extension MessagesViewController : UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return model.rowCount
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cellModel = model.modelForRow(row: indexPath.row)
		let identifier : String
		switch cellModel.messageType {
		case .incoming :
			identifier = incomingCellIdentifier
			break
		case .outgoing:
			identifier = outgoingCellIdentifier
			break
		}
		let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
		(cell as! MessageTableViewCell).configure(withModel: cellModel)
		return cell
	}
	
}

extension MessagesViewController : UITableViewDelegate {
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		if targetContentOffset.pointee.y < scrollView.contentOffset.y {
			self.keyboardInputView.stopEditing()
		}
	}
}

protocol MessageTableViewCell : class {
	func configure(withModel model : MessageTableViewCellModel)
}
