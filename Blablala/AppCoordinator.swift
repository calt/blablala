//
//  AppCoordinator.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit

final class AppCoordinator {
	
	static let shared = AppCoordinator()
	
	private init() {}
	
	private let _userKey = "user"
	private var _user : AppUser?
	var user : AppUser? {
		get {
			if _user == nil, let data = UserDefaults.standard.data(forKey: _userKey) {
				_user =  NSKeyedUnarchiver.unarchiveObject(with: data) as? AppUser
			}
			return _user
		}
		set {
			_user = newValue
			if  let val = newValue {
				UserDefaults.standard.setValue(NSKeyedArchiver.archivedData(withRootObject: val), forKeyPath: _userKey)
			} else {
				UserDefaults.standard.removeObject(forKey: _userKey)
			}
			UserDefaults.standard.synchronize()
		}
	}
	
	private let _stateKey = "state"
	private var _state : AppState?
	var state : AppState? {
		get {
			if _state == nil, let raw = UserDefaults.standard.string(forKey: _stateKey) {
				_state = AppState(rawValue: raw)
			}
			return _state
		}
		set {
			_state = newValue
			if _state == nil {
				UserDefaults.standard.removeObject(forKey: _stateKey)
			} else {
				UserDefaults.standard.setValue(newValue?.rawValue, forKeyPath: _stateKey)
			}
		}
	}
	
	func getInitialViewController() -> UIViewController {
		if state == nil {
			state = .nicknameSelection
		}
		
		switch state! {
		case .nicknameSelection:
			mainNavigationController.pushViewController(newNicknameViewController(), animated: false)
			return mainNavigationController
		case .messages :
			mainNavigationController.pushViewController(newMessagesViewController(), animated: false)
			return mainNavigationController
		}
	}
	
	lazy var mainNavigationController : UINavigationController = {
		return UINavigationController()
	}()
	
	private lazy var messageService : MessageServiceProtocol = {
		return MessageService()
	}()
	
	private func newNicknameViewController() -> NicknameViewController {
		let model = NicknameViewModel()
		return NicknameViewController(model: model)
	}
	
	private func newMessagesViewController() -> MessagesViewController {
		assert(user != nil, "User should not be nil while creating this Messages VC")
		let model = MessagesViewModel(service: messageService, user : user!)
		return MessagesViewController(model: model)
	}
	
	func showMessages(for user : AppUser) {
		guard state != .messages else {
			return
		}
		self.user = user
		let messagesViewController = newMessagesViewController()
		mainNavigationController.pushViewController(messagesViewController, animated: true)
		mainNavigationController.setViewControllers([messagesViewController], animated: false) // remove nicknameselectionvc from the stack
		state = .messages
	}
	
	func logout() {
		guard state != .nicknameSelection else {
			return
		}
		var vcs = mainNavigationController.viewControllers
		let nicknameViewController = newNicknameViewController()
		vcs.insert(nicknameViewController, at: 0)
		mainNavigationController.viewControllers = vcs
		mainNavigationController.setViewControllers(vcs, animated: false)
		mainNavigationController.popToViewController(nicknameViewController, animated: false) // shows an intermediary blank view if animated, couldn't figure out why.
		state = .nicknameSelection
	}
}

enum AppState : String {
	case nicknameSelection = "NicknameSelection"
	case messages = "Messages"
}
