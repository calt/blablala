//
//  Date+Formatted.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 24/02/2017.
//  Copyright © 2017 dembrew. All rights reserved.
//

import UIKit

extension Date {

	var formatted : String {
		let formatter = DateFormatter()
		formatter.amSymbol = "am".localized
		formatter.pmSymbol = "pm".localized
		formatter.dateFormat = "hh:mm a, dd/MM/yyyy"
		return formatter.string(from: self)
	}

}
