//
//  String+Localized.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 dembrew. All rights reserved.
//

import UIKit

extension String {
	var localized : String {
		return String(NSLocalizedString(self, comment: ""))
	}
}
