//
//  UIButton+Gradient.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit

extension UIButton {
	
	func applyGradient(colors: [UIColor], locations: [NSNumber]? = nil) -> Void {
		let gradient: CAGradientLayer = CAGradientLayer()
		gradient.frame = self.bounds
		gradient.colors = colors.map { $0.cgColor }
		gradient.locations = locations
		self.layer.insertSublayer(gradient, at: 0)
	}
	
}
