//
//  MessageService.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import Alamofire
import SwiftyJSON

protocol MessageServiceProtocol {
	func getMessages(callback : @escaping (_ messages : [Message]?, _ success: Bool, _ error : String?) -> ())
	func sendMessage(_ text : String, sender : User, callback : @escaping (_ message : Message?, _ success: Bool, _ error : String?) -> ())
}

class MessageService : MessageServiceProtocol {
	
	private let rootURL = "https://jsonblob.com/api/jsonBlob/"
	private let messagesPath = "3cf871b2-f7cd-11e6-95c2-115605632e53"
	
	func getMessages(callback : @escaping  (_ messages : [Message]?, _ success: Bool, _ error : String?) -> ()) {
		
		let url = rootURL + messagesPath
		
		Alamofire.request(url, method: .get).responseJSON { (response) in
			switch response.result {
			case .success:
				var messages : [Message] = []
				if let value = response.result.value,
					let jsonArray = JSON(value)["messages"].array {
					for messageJson in jsonArray {
						messages.append(Message(json: messageJson))
					}
				}
				callback(messages, true, nil)
				return
			case .failure(_):
				callback(nil, false, "Error loading messages")
				return
			}
		}
	}
	
	func sendMessage(_ text : String, sender : User, callback : @escaping (_ message : Message?, _ success: Bool, _ error : String?) -> ()) {
		let message = Message(identifier: Int(arc4random()), text: text, time: Date(), user: sender)
		callback(message, true, nil)
	}
}
