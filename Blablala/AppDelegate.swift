//
//  AppDelegate.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		window = UIWindow()
		setupGlobals()
		loadInitialViewController()
		return true
	}

	private func loadInitialViewController() {
		let initialVC = AppCoordinator.shared.getInitialViewController()
		window?.rootViewController = initialVC
		window?.makeKeyAndVisible()
	}
	
	private func setupGlobals() {
		let navbarAppearence = UINavigationBar.appearance()
		navbarAppearence.barTintColor = #colorLiteral(red: 0.6170199513, green: 0.7693166137, blue: 0.9161146283, alpha: 1)
		navbarAppearence.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
	}

}
