//
//  NicknameViewModelTests.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 25/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import XCTest
@testable import Blablala


class NicknameViewModelTests: XCTestCase {
	
	var model : NicknameViewModel!
	
	override func setUp() {
		super.setUp()
		model = NicknameViewModel()
	}
	
	func testNicknameViewModel() {		
		
		let exp1 = expectation(description: "1 character should show error")
		model.showError = { show in
			assert(show)
			exp1.fulfill()
		}
		model.nickname = "x"
		
		let exp2 = expectation(description: "blank character should show error")
		model.showError = { show in
			assert(show)
			exp2.fulfill()
		}
		model.nickname = ""
		
		
		let exp3 = expectation(description: "nil character should show error")
		model.showError = { show in
			assert(show)
			exp3.fulfill()
		}
		model.nickname = nil
		
		
		let exp4 = expectation(description: "all whitespace character should show error")
		model.showError = { show in
			assert(show)
			exp4.fulfill()
		}
		model.nickname = "      "
		
		
		let exp5 = expectation(description: "3+ characters should not show error")
		model.showError = { show in
			XCTAssertFalse(show)
			exp5.fulfill()
		}
		model.nickname = "AAA"
		
		waitForExpectations(timeout: 5, handler: { error in
			XCTAssertNil(error, "Something went wrong")
		})
		
	}
}
