//
//  MessagesViewModelTests.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 25/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import XCTest
@testable import Blablala


class MessagesViewModelTests: XCTestCase {
	
	var model : MessagesViewModel!
	var appUser : AppUser!
	var service : MockService!
	
	override func setUp() {
		super.setUp()
		appUser = AppUser(identifier: 0, nickname: "Bird Person")
		service = MockService()
		model = MessagesViewModel(service: service, user: appUser)
	}
	
	func testLoadMessages() {
		
		let loadMessagesExpectiation = expectation(description: "Load Messages")
		
		model.modelDidUpdate = {
			assert(self.model.rowCount == 4)
			assert(self.model.modelForRow(row: 0).messageType == .incoming)
			assert(self.model.modelForRow(row: 1).messageType == .incoming)
			assert(self.model.modelForRow(row: 2).messageType == .incoming)
			assert(self.model.modelForRow(row: 3).messageType == .outgoing)
			loadMessagesExpectiation.fulfill()
		}
		
		model.viewLoaded()
		
		waitForExpectations(timeout: 5, handler: { error in
			XCTAssertNil(error, "Something went wrong")
		})
	}
	
	
	func testSendMessages() {
		
		let sendMessageExpectiation = expectation(description: "Send Message")
		
		model.messageAdded = {
			assert(self.model.rowCount == 1)
			let cellModel = self.model.modelForRow(row: 0)
			assert(cellModel.messageText == "Hooraay")
			assert(cellModel.messageType == .outgoing)
			assert(cellModel.nicknameText == "Bird Person")
			sendMessageExpectiation.fulfill()
		}
		
		model.sendMessage("Hooraay")
		
		waitForExpectations(timeout: 5, handler: { error in
			XCTAssertNil(error, "Something went wrong")
		})
	}
	
}

class MockService : MessageServiceProtocol {
	
	let appUser = AppUser(identifier: 0, nickname: "Bird Person")
	
	func getMessages(callback: @escaping ([Message]?, Bool, String?) -> ()) {
		let user1 = User(identifier: 99, nickname: "Testy McTesterson")
		
		
		let messages = [Message(identifier: 1, text: "Message 1", time: Date.init(timeIntervalSince1970: 9990), user: user1),
		                Message(identifier: 1, text: "Message 2", time: Date.init(timeIntervalSince1970: 9991), user: user1),
		                Message(identifier: 1, text: "Message 3", time: Date.init(timeIntervalSince1970: 9992), user: user1),
		                Message(identifier: 1, text: "Message 4", time: Date.init(timeIntervalSince1970: 9993), user: appUser.user)]
		
		callback(messages, true, nil)
	}
	
	func sendMessage(_ text: String, sender: User, callback: @escaping (Message?, Bool, String?) -> ()) {
		let message = Message(identifier: Int(arc4random()), text: text, time: Date(), user: appUser.user)
		callback(message, true, nil)
	}
}
