//
//  MessageServiceTests.swift
//  Blablala
//
//  Created by Çağdaş Altınkaya on 23/02/2017.
//  Copyright © 2017 Çağdaş Altınkaya. All rights reserved.
//

import XCTest
@testable import Blablala


class MessageServiceTests: XCTestCase {
	
	var messageService : MessageService!
    
    override func setUp() {
        super.setUp()
        messageService = MessageService()
    }
	
	func testMessageService() {
		
		let messageExpectiation = expectation(description: "Load Messages")
	
		messageService.getMessages { messages, success, error in
			
			XCTAssertTrue(success)
			XCTAssertNotNil(messages)
			XCTAssertNil(error)
			
			messageExpectiation.fulfill()
		}

		waitForExpectations(timeout: 5, handler: { error in
			XCTAssertNil(error, "Something went wrong")
		})
	}
    
    override func tearDown() {
        super.tearDown()
    }
    
	
    
}
